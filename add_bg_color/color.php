<?php
add_action('wp_head','add_color');

/**
 * Function to add background color to body tag i.e. <body>
 */
function add_color()
{
	$color_settings = get_option( "color_theme_settings" );
	?>
	<style type="text/css">
	body
	{	
		background: <?php echo $color_settings['default-color']; ?> no-repeat !important;
	}
	</style>
	<?php 	
}
?>
