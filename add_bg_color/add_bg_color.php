<?php
/*
Plugin Name: Background Color
Plugin URI: http://cedcoss.com
Description: This plugin is created to change the background of the theme.
Version: 9.1
Author: Surabhi Srivastava
Author URI: http://surabhi.com
License: GPL2
*/
?>
<?php 


/**
 * include the contents of some other file.
 */
include('color.php');



/**
 * Color picker is added to select any color from it
 */
function admin_color_enqueue()
{
	if(is_admin())
	{
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script('jquery');
		wp_enqueue_script( 'meta-color-js', plugin_dir_url(__FILE__).'/js/meta-color.js', array( 'wp-color-picker' ),true,true);
	}
} // End example_color_enqueue()
add_action( 'admin_enqueue_scripts', 'admin_color_enqueue' );



add_action('init','theme_admin_init');
add_action( 'admin_menu', 'color_settings_page_init' );

/**
 * A function which which will add option to assign color in the variable $settings
 * if initially the settings variable is empty i.e. no option is created for it, the new option will be created for it with a default value.
 */
function theme_admin_init()
{
	$settings = get_option( "color_theme_settings" );
	if ( empty( $settings ) ) {
		$settings = array(
						'default-color' => 'e6e6e6',
					);
		add_option( "color_theme_settings", $settings, '', 'yes' );
	}
}

/**
 * Function to initialize color settings page at admin menu. This function will add the color settings page to the admin panel.
 */
function color_settings_page_init()
{
	$settings_page = add_theme_page( 'Color Settings', ' Color Settings', 'edit_theme_options', 'color-settings', 'color_settings_page' );
	add_action( "load-{$settings_page}", 'color_load_settings_page' );
}


/**
 * Function to redirect to the function which will save the content if the user has requested to update the settings.
 */
function color_load_settings_page()
{
	if ( $_POST["color-settings-submit"] == 'Y' )
	{
		check_admin_referer( "color-settings-page" );
		color_save_theme_settings();
		$url_parameters = 'updated=true';
		wp_redirect(admin_url('themes.php?page=color-settings&'.$url_parameters));
		exit;
		
	}
}


/**
 * Function which will save all the content given by user to a specific variable and update the option table as per given value.
 */
function color_save_theme_settings()
{
	$settings['default-color'] = $_POST['meta-color'];
	$updated = update_option( "color_theme_settings", $settings );
}


/**
 * Function which will show the settings page from where you can change the background color of the activated theme.
 */
function color_settings_page()
{
	$settings = get_option( "color_theme_settings" );
	?>
	<div class="wrap">
		<h2>Background Color Settings</h2>
		
		<?php
			if ( 'true' == esc_attr( $_GET['updated'] ) ) echo '<div class="updated" ><p>Theme Settings updated.</p></div>';
		?>

		<div id="poststuff">
			<form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>">
				<?php
				wp_nonce_field( "color-settings-page" ); 
				?>
				<table>
							
								<?php 
								$color_settings = get_option( "color_theme_settings" );
								echo '<tr><td><label for="meta-color" class="example-row-title">Color Picker-1</label>';
								echo '<td><input name="meta-color" type="text" value="'.$color_settings['default-color'].'" class="meta-color" /></td></tr>';
								?>
								<span class="description">Choose a color</span>														
								<?php echo '</table>';
								?>
				<p class="submit" style="clear: both;">
					<input type="submit" name="Submit"  class="button-primary" value="Update Settings" />
					<input type="hidden" name="color-settings-submit" value="Y" />
				</p>
			</form>
			
		</div>

	</div>
<?php }?>








