<div id="sidebar">
<ul>
	<?php if( !function_exists('register_sidebar') || !dynamic_sidebar() ) : ?>
		<li><?php get_search_form();?></li>
	
	<?php if ( is_404() || is_category() || is_day() || is_month() ||
						is_year() || is_search() || is_paged() ):
	?>
	   
	<?php 
		if(is_404())
		{
		
		}
			elseif(is_category())
			{
			printf(__('You are currently viewing archieves from %s category'), single_cat_title('',true));
			}
		 		elseif(is_day())
		 		{
		 		printf(__('You are currently browsing the <a href="%1$s/">%2$s</a> blog archives for the day %3$s.'), get_bloginfo('url'), get_bloginfo('name'), get_the_time(__('l, F jS, Y')));
		 		}
		 			elseif (is_month())
		 			{
		 			printf(__('You are currently browsing the <a href="%1$s/">%2$s</a> blog archives for the month %3$s.'), get_bloginfo('url'), get_bloginfo('name'), get_the_time(__('F,Y')));	
		 			}
						elseif (is_month())
		 				{
		 				printf(__('You are currently browsing the <a href="%1$s/">%2$s</a> blog archives for the year %3$s.'), get_bloginfo('url'), get_bloginfo('name'), get_the_time(__('Y')));	
		 				}
		 					elseif(is_search())
		 					{
		 						printf(__('You have searched the <a href="%1$s/">%2$s</a> blog archives for <strong>&#8216;%3$s&#8217;</strong>. If you are unable to find anything in these search results, you can try one of these links.'), get_bloginfo('url'), get_bloginfo('name'), wp_specialchars(get_search_query(), true));
		 					}
		 						elseif (isset($_GET['paged']) && !empty($_GET['paged']))
		 						{
		 						printf(__('You are currently browsing the <a href="%1$s/">%2$s</a> blog archives.'), get_bloginfo('url'), get_bloginfo('name'));
		 						}
		 endif; 
		 
		 
		 endif;
		// dynamic_sidebar();
		 ?>

</ul>
</div>
					<!-- end #sidebar -->