<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<meta name="keywords" content="<?php bloginfo('html_type'); ?>" />
<meta name="description" content="<?php bloginfo('html_type'); ?>" />
<meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" /> 
<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<div id="wrapper">
	<div id="wrapper-bgbtm">
		<div id="header">
			<div id="logo">
				<h1><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
				<p><?php bloginfo('description'); ?></p>
			</div>
		</div>
		<!-- end #header -->
		<?php wp_nav_menu( 
					array( 
						'theme_location' => '',
						'menu' => 'Menu_name', 
						'container_id' => 'menu', 
						'container' => 'div', 
						'container_class' => '') );?>
		<!-- end #menu -->