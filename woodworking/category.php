<?php get_header();?>
<div id="content">
<div id="category">
<?php if(have_posts()): ?>
<?php while(have_posts()) : the_post();?>
	<div class="post">
	<h2 class="title"><a href="<?php the_permalink();?>"><?php  the_title(); ?> </a></h2>
	<p class="meta">Posted By <?php the_author();?> On <?php the_time(__('F jS, Y'));?>
	<br><?php comments_popup_link('No Comments>>','1 Comment>>','%Comments>>');?></p>
	</div>
		<div class="content">
		<?php the_content('<p class="serif">' . __('Read the rest of this page &raquo;') . '</p>');?>
		</div>
		<p class="postmetadata"><?php the_tags(__('Tags:') . ' ', ', ', '<br />'); ?> <?php printf(__('Posted in %s'), get_the_category_list(', ')); ?> | <?php edit_post_link(__('Edit'), '', ' '); ?></p>
	</div>
<?php endwhile;?>
<?php else : ?>
<p><?php echo(__('Sorry there is no posts'));?></p>
<?php endif;?>
</div>
<?php get_sidebar();?>
</div>
<div style="clear:both;">
</div>
<?php get_footer();?>