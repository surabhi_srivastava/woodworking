<?php 
get_header();
?>
<div id="content" class="narrowcolumn" role="main">
<?php if(have_posts()) :?>
	<?php while(have_posts()): the_post();?>
			<div class="post" id="post-<?php the_ID();?>">
			<h1>test <?php the_title();?></div>
			<p>By <?php the_author();?> at <?php the_time(__('F jS, Y'))?></p>
			<div class="entry">
				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => '<p><strong>' . __('Pages:', 'kubrick') . '</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
			</div>
	<?php endwhile;?>
<?php else :?>
<p><?php echo __('Sorry there is no post.');?></p>
<?php endif; ?>

<?php edit_post_link(__('Edit')); ?>

<?php comments_template(); ?> 
	
</div>
<?php get_sidebar();?>
<div style="clear:both;">
</div>
<?php get_footer(); ?>