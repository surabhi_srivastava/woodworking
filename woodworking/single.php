<?php get_header(); ?>
<div id="page">
	<div id="page-bgtop">
		<div id="page-bgbtm">
			<div id="content">
				
				<?php if(have_posts()): ?>
					
					<?php while(have_posts()): the_post(); ?>
					
						<div class="post">
							<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?> </a></h2>
							<p class="meta">Posted by <?php the_author() ?> on <?php the_time(__('F jS, Y')) ?>
								&nbsp;&bull;&nbsp; <?php comments_popup_link(__('No Comments &#187;'), __('1 Comment &#187;'), __('% Comments &#187;'), '', __('Comments Closed') ); ?> &nbsp;&bull;&nbsp; <a href="<?php the_permalink();?>" class="permalink"><?php echo __('Full article');?></a></p>
							<div class="entry">
								<?php the_content(); ?>
							</div>
							<p class="postmetadata"><?php the_tags(__('Tags:') . ' ', ', ', '<br />'); ?> <?php printf(__('Posted in %s'), get_the_category_list(', ')); ?> | <?php edit_post_link(__('Edit'), '', ' '); ?></p>
							<p class="postmetadata alt">
								<small>
									<?php /* This is commented, because it requires a little adjusting sometimes.
										You'll need to download this plugin, and follow the instructions:
										http://binarybonsai.com/wordpress/time-since/ */
										/* $entry_datetime = abs(strtotime($post->post_date) - (60*120)); $time_since = sprintf(__('%s ago', 'kubrick'), time_since($entry_datetime)); */ ?>
									<?php printf(__('This entry was posted on %1$s at %2$s and is filed under %3$s.', 'kubrick'), get_the_time(__('l, F jS, Y', 'kubrick')), get_the_time(), get_the_category_list(', ')); ?>
									<?php printf(__("You can follow any responses to this entry through the <a href='%s'>RSS 2.0</a> feed.", "kubrick"), get_post_comments_feed_link()); ?> 
			
									<?php if ( comments_open() && pings_open() ) {
										// Both Comments and Pings are open ?>
										<?php printf(__('You can <a href="#respond">leave a response</a>, or <a href="%s" rel="trackback">trackback</a> from your own site.', 'kubrick'), get_trackback_url()); ?>
			
									<?php } elseif ( !comments_open() && pings_open() ) {
										// Only Pings are Open ?>
										<?php printf(__('Responses are currently closed, but you can <a href="%s" rel="trackback">trackback</a> from your own site.', 'kubrick'), get_trackback_url()); ?>
			
									<?php } elseif ( comments_open() && !pings_open() ) {
										// Comments are open, Pings are not ?>
										<?php _e('You can skip to the end and leave a response. Pinging is currently not allowed.', 'kubrick'); ?>
			
									<?php } elseif ( !comments_open() && !pings_open() ) {
										// Neither Comments, nor Pings are open ?>
										<?php _e('Both comments and pings are currently closed.', 'kubrick'); ?>
			
									<?php } edit_post_link(__('Edit this entry', 'kubrick'),'','.'); ?>
			
								</small>
							</p>
						</div>
						<?php comments_template(); ?>
						
					<?php endwhile; ?>
					
				<?php else: ?>
					<p><?php echo __('Sorry there is no post.'); ?></p>
				<?php endif; ?>
				
				
				
				
			</div>
			<!-- end #content -->
			<?php  get_sidebar();?>
			<div style="clear: both;">&nbsp;</div>
		</div>
	</div>
</div>
<!-- end #page -->
<?php get_footer(); ?>
