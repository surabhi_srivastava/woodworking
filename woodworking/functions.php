<?php 
	add_theme_support( 'menus' );
	
if ( function_exists('register_sidebar') )
register_sidebar(array(
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-header' ); 

class First_Widget extends WP_Widget {
	function __construct() {
		parent::__construct(
			'first_widget', // Base ID
			'Blogroll', // Name
			array( 'description' => __( 'Blogroll', 'text_domain' ), ) // Args
		);
	}
	public function widget( $args, $instance) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$name= $instance['cat'] ;
		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title .  $after_title;
			echo $name;
			
		?>
		
		<iframe width="205" height="205" src="//www.youtube.com/embed/VhRwuWp4MQ8" frameborder="0" allowfullscreen></iframe>
		<?php 
		echo $after_widget;
	}
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
			$name = $instance['cat'];
			$show_info = isset( $instance['show_info'] ) ? $instance['show_info'] : false; 
		}
		else {
			$title = __( 'New title', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_name( 'cat' ); ?>"><?php _e( 'Category:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'cat' ); ?>" name="<?php echo $this->get_field_name( 'cat' ); ?>" type="text" value="<?php echo esc_attr( $name ); ?>" />
		</p>
		<p>  
    	<input class="checkbox" type="checkbox" <?php checked( $instance['show_info'], true ); ?> id="<?php echo $this->get_field_id( 'show_info' ); ?>" name="<?php echo $this->get_field_name( 'show_info' ); ?>" />   
    	<label for="<?php echo $this->get_field_id( 'show_info' ); ?>"><?php _e('Display info publicly?', 'example'); ?></label>  
		</p>  
		<p>
		<select id="<?php echo $this->get_field_id('posttype'); ?>" name="<?php echo $this->get_field_name('posttype'); ?>" class="widefat" style="width:100%;">
    <option <?php selected( $instance['posttype'], 'Option 1'); ?> value="Option 1">Option 1</option>
    <option <?php selected( $instance['posttype'], 'Option 2'); ?> value="Option 2">Option 2</option> 
    <option <?php selected( $instance['posttype'], 'Option 3'); ?> value="Option 3">Option 3</option>   
</select>
		</p>
		<?php 
	}
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['cat']= ( !empty( $new_instance['cat'] ) ) ? strip_tags( $new_instance['cat'] ) : '';
		$instance['show_info'] = $new_instance['show_info'];  
		return $instance;
	}

} // class Foo_Widget
function myplugin_register_widgets() {
	register_widget( 'First_Widget' );
}

add_action( 'widgets_init', 'myplugin_register_widgets' );
