<?php get_header(); ?>
<div id="page">
	<div id="page-bgtop">
		<div id="page-bgbtm">
			<div id="content">
				
				<?php if(have_posts()): ?>
					
					<?php while(have_posts()): the_post(); ?>
					
						<div class="post">
							<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?> </a></h2>
							<p class="meta">Posted by <?php the_author() ?> on <?php the_time(__('F jS, Y')) ?>
								&nbsp;&bull;&nbsp; <?php comments_popup_link(__('No Comments &#187;'), __('1 Comment &#187;'), __('% Comments &#187;'), '', __('Comments Closed') ); ?> &nbsp;&bull;&nbsp; <a href="<?php the_permalink();?>" class="permalink"><?php echo __('Full article');?></a></p>
							<div class="entry" style="float:left;">
							<?php if(has_post_thumbnail()) : the_post_thumbnail(); endif;?>
							</div>
							<div style="float:left;  width:60%; padding:10px;"> <?php the_content(); ?> </div>					
							<div style="clear:both;"></div>						
							<p class="postmetadata"><?php the_tags(__('Tags:') . ' ', ', ', '<br />'); ?> <?php printf(__('Posted in %s'), get_the_category_list(', ')); ?> | <?php edit_post_link(__('Edit'), '', ' '); ?></p>
						</div>
						
					<?php endwhile; ?>
					
				<?php else: ?>
					<p><?php echo __('Sorry there is no post.'); ?></p>
				<?php endif; ?>
				
				
				
				
			</div>
			<!-- end #content -->
			<?php  get_sidebar();?>
			<div style="clear: both;">&nbsp;</div>
		</div>
	</div>
</div>
<!-- end #page -->
<?php get_footer(); ?>
